resource "aws_s3_bucket" "s3-job-offer-bucket-<ID-GROUP-TP>" {
  bucket = "s3_user_bucket_name"
  acl    = "public-read"
  force_destroy = true 
}

resource "aws_s3_bucket_object" "JobOffersObject" {
  bucket = aws_s3_bucket.s3-job-offer-bucket-<ID-GROUP-TP>.id
  key    = "job_offers/"
  source = "/dev/null"
}

resource "aws_s3_bucket_object" "RawObject" {
  bucket = aws_s3_bucket.s3-job-offer-bucket-<ID-GROUP-TP>.id
  key    = "job_offers/raw/"
  source = "/dev/null"
}